<!DOCTYPE html>
<html>
<head>
    <title>Connexion</title>
</head>
<body>
<h1>Connexion</h1>
<form method="post" action="./routeur/authentificationRouteur.php">
    <input type="text" name="email" placeholder="Adresse e-mail">
    <input type="password" name="mot_de_passe" placeholder="Mot de passe">
    <label for="type">Type d'utilisateur :</label>
    <select name="type" id="type">
        <option value="directeur">Directeur</option>
        <option value="formateur">Formateur</option>
        <option value="stagiaire">Stagiaire</option>
    </select>
    <button type="submit">Se connecter</button>
</form>
</body>
</html>
